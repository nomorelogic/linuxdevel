{
    Pas2JS Time Tracking Demo
    Copyright (c) Miguel Bebensee 2018

    Demo avaible at: https://www.devstructor.com/demos/pas2js-time/
}

unit unitBookingData;

{$mode objfpc}{$H+}

interface

uses
  SysUtils, Web, JS, Classes, unitModel;

type

  { TBookingData }

  TBookingData = class(TObject)
  private
    FBookings: TList;

    function GetBooking(Index: Integer): TBooking;
    function GetBookingCount: Integer;
    function GetIsOpened: Boolean;

    procedure LoadData;
  public
    constructor Create;

    procedure Book;
    procedure DeleteAll;

    property BookingCount: Integer read GetBookingCount;
    property Booking[Index: Integer]: TBooking read GetBooking;
    property IsOpened: Boolean read GetIsOpened;
  end;

implementation

{ TBookingData }

function TBookingData.GetBookingCount: Integer;
begin
  Result:=FBookings.Count;
end;

function TBookingData.GetIsOpened: Boolean;
begin
  Result:=(GetBookingCount > 0) and (Booking[BookingCount -1].IsOpened);
end;

procedure TBookingData.LoadData;
var LastID: Integer = 0;
  i: Integer;
begin
  if window.localStorage.getItem('generator') <> Null then
    LastID:=StrToInt(window.localStorage.getItem('generator'));

  if LastID = 0 then Exit;

  for i := 1 to LastID do FBookings.Add(TBooking.Create(i));
end;

function TBookingData.GetBooking(Index: Integer): TBooking;
begin
  Result:=TBooking(FBookings.Items[Index]);
end;

constructor TBookingData.Create;
begin
  FBookings:=TList.Create;
  LoadData;
end;

procedure TBookingData.Book;
begin
  if GetIsOpened then
    Booking[BookingCount - 1].Book
  else FBookings.Add(TBooking.Create);
end;

procedure TBookingData.DeleteAll;
var
  i: Integer;
begin
  LoadData;

  for i := 0 to Pred(BookingCount) do Booking[i].Delete;
  window.localStorage.removeItem('generator');

  FBookings.Clear;
end;

end.

