{
    Pas2JS Time Tracking Demo
    Copyright (c) Miguel Bebensee 2018

    Demo avaible at: https://www.devstructor.com/demos/pas2js-time/
}

unit unitModel;

{$mode objfpc}{$H+}

interface

uses
  SysUtils, Web, JS, dateutils;

type

  { TBooking }

  TBooking = class(TObject)
  private
    FID: Integer;
    FStart: TDateTime;
    FStop: TDateTime;
    function GetIsOpened: Boolean;
    procedure SetID(AValue: Integer);
    procedure SetStart(AValue: TDateTime);
    procedure SetStop(AValue: TDateTime);
    function GenID: Integer;
  public
    constructor Create;
    constructor Create(AID: Integer);

    procedure Load;
    procedure Save;
    procedure Delete;

    procedure Book;
    function GetWorkingTimeString: String;
  published
    property ID: Integer read FID write SetID;
    property Start: TDateTime read FStart write SetStart;
    property Stop: TDateTime read FStop write SetStop;
    property IsOpened: Boolean read GetIsOpened;
  end;

implementation

{ TBooking }

procedure TBooking.SetStart(AValue: TDateTime);
begin
  if FStart=AValue then Exit;
  FStart:=AValue;
  Save;
end;

procedure TBooking.SetID(AValue: Integer);
begin
  if FID=AValue then Exit;
  FID:=AValue;
  Save;
end;

function TBooking.GetIsOpened: Boolean;
begin
  Result:=(FStop = 0) or (FStart = 0);
end;

procedure TBooking.SetStop(AValue: TDateTime);
begin
  if FStop=AValue then Exit;
  FStop:=AValue;
  Save;
end;

function TBooking.GenID: Integer;
var LastID: Integer = 0;
begin
  if window.localStorage.getItem('generator') <> Null then
    LastID:=StrToInt(window.localStorage.getItem('generator'));

  Result:=LastID + 1;
  window.localStorage.setItem('generator', IntToStr(Result));
end;

constructor TBooking.Create;
begin
  FID:=GenID;
  FStart:=0;
  FStop:=0;

  Book;
end;

constructor TBooking.Create(AID: Integer);
begin
  FID:=AID;
  Load;
end;

procedure TBooking.Load;
begin
  FStart:=0;
  FStop:=0;

  if window.localStorage.getItem(IntToStr(ID)+'-start') <> Null then
    FStart:=StrToDateTime(window.localStorage.getItem(IntToStr(ID)+'-start'));

  if window.localStorage.getItem(IntToStr(ID)+'-stop') <> Null then
    FStop:=StrToDateTime(window.localStorage.getItem(IntToStr(ID)+'-stop'));
end;

procedure TBooking.Save;
begin
  window.localStorage.setItem(IntToStr(ID)+'-start', DateTimeToStr(FStart));
  if FStop > 0 then window.localStorage.setItem(IntToStr(ID)+'-stop', DateTimeToStr(FStop))
  else window.localStorage.removeItem(IntToStr(ID)+'-stop');
end;

procedure TBooking.Delete;
begin
  window.localStorage.removeItem(IntToStr(ID)+'-start');
  window.localStorage.removeItem(IntToStr(ID)+'-stop');
end;

procedure TBooking.Book;
begin
  if FStart = 0 then SetStart(Now) else SetStop(Now);
end;

function TBooking.GetWorkingTimeString: String;
var
  TotalSeconds, Days, Hours, Minutes, Seconds: Integer;
begin
  TotalSeconds:=SecondsBetween(Now, FStart);

  Days:=TotalSeconds div (24 * 60 * 60);
  TotalSeconds:=TotalSeconds mod (24 * 60 * 60);

  Hours:=TotalSeconds div (60 * 60);
  TotalSeconds:=TotalSeconds mod (60 * 60);

  Minutes:=TotalSeconds div 60;
  Seconds:=TotalSeconds mod 60;

  if Days > 0 then Result:=Result + IntToStr(Days) + 'd ';
  if Hours > 0 then Result:=Result + IntToStr(Hours) + 'h ';
  if Minutes > 0 then Result:=Result + IntToStr(Minutes) + 'm ';
  if Seconds > 0 then Result:=Result + IntToStr(Seconds) + 's ';
end;

end.

