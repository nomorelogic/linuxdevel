{
    Pas2JS Time Tracking Demo
    Copyright (c) Miguel Bebensee 2018

    Demo avaible at: https://www.devstructor.com/demos/pas2js-time/
}

program TimeTracking;

uses
  SysUtils, Web, JS, unitModel, unitBookingData;

type

  { TTimeForm }

  TTimeForm = class(TObject)
    lblTime: TJSElement;
    lblActiveBooking: TJSElement;
    btnBook: TJSHTMLElement;
    btnDelete: TJSHTMLElement;
  private
    FBookingData: TBookingData;

    procedure ShowText(Text, AlertClass: String);

    procedure UpdateTime;
    procedure UpdateActiveBooking;
    procedure UpdateBookButton;
    procedure RenderBookings;
    procedure HideDlgDelete;

    function BookClick(Event: TJSMouseEvent): Boolean;
    function DeleteClick(Event: TJSMouseEvent): Boolean;
  public
    constructor Create;

    procedure ShowInfo(Text: String; Success: Boolean = False);
    procedure ShowWarning(Text: String);
    procedure ShowError(Text: String);
  end;

{ TTimeForm }

procedure TTimeForm.ShowText(Text, AlertClass: String);
var
  pnlMessage, lblMessage, btnClose: TJSElement;
begin
  //Set the alert properties
  lblMessage:=document.createElement('div');
  lblMessage['class']:='alert alert-dismissible fade show ' + AlertClass;
  lblMessage['role']:='alert';

  //Create a close button
  btnClose:=document.createElement('button');
  btnClose['type']:='button';
  btnClose['class']:='close';
  btnClose['data-dismiss']:='alert';
  btnClose['aria-label']:='Close';
  btnClose.innerHTML:='<span aria-hidden="true">&times;</span>';


  //Set the text and append the close button
  lblMessage.innerText:=Text;
  lblMessage.append(btnClose);

  pnlMessage:=document.getElementById('pnlMessages');
  pnlMessage.innerHTML:='';
  pnlMessage.append(lblMessage);
end;

procedure TTimeForm.UpdateTime;
begin
  //Updates the time in the top right corner
  lblTime.innerText:=TimeToStr(Time);
  UpdateActiveBooking;
  window.setTimeout(@UpdateTime, 1000);
end;

procedure TTimeForm.UpdateActiveBooking;
var WorkingTime: TDateTime;
begin
  //Updates the working time of the last active booking
  lblActiveBooking.innerHTML:='';
  if not FBookingData.IsOpened then Exit;

  lblActiveBooking.innerText:='Active booking: ' + FBookingData.Booking[FBookingData.BookingCount-1].GetWorkingTimeString;
  lblActiveBooking.innerHTML:=lblActiveBooking.innerHTML + '<hr>';
end;

procedure TTimeForm.UpdateBookButton;
begin
  //Switches buttons between Start and Stop
  if FBookingData.IsOpened then
  begin
    btnBook.innerText:='Stop';
    btnBook['class']:='btn btn-success';
  end
  else
  begin
    btnBook.innerText:='Start';
    btnBook['class']:='btn btn-primary';
  end;
end;

procedure TTimeForm.RenderBookings;
var
  i: Integer;
  tblBookings: TJSElement;
  tblRow, colID, colStart, colStop: TJSElement;
begin
  //Generates the Table
  UpdateBookButton;
  UpdateActiveBooking;

  tblBookings:=document.getElementById('tblBookings');
  tblBookings.innerHTML:='';

  for i := Pred(FBookingData.BookingCount) downto 0 do
  begin
    if FBookingData.Booking[i].IsOpened then Continue;

    tblRow:=document.createElement('tr');

    colID:=document.createElement('th');
    colID.innerText:=IntToStr(FBookingData.Booking[i].ID);

    colStart:=document.createElement('td');
    colStart.innerText:=DateTimeToStr(FBookingData.Booking[i].Start);

    colStop:=document.createElement('td');
    colStop.innerText:=DateTimeToStr(FBookingData.Booking[i].Stop);

    tblRow.append(colID);
    tblRow.append(colStart);
    tblRow.append(colStop);
    tblBookings.append(tblRow);
  end;
end;

procedure TTimeForm.HideDlgDelete; assembler; //Hides the confirmation dialog (delete all)
asm
  $('#dlgDelete').modal('hide');
end;

function TTimeForm.BookClick(Event: TJSMouseEvent): Boolean;
begin
  FBookingData.Book;
  RenderBookings;

  if FBookingData.IsOpened then
    ShowInfo('New booking was added', true)
  else
    ShowInfo('Your booking was closed', true);
end;

function TTimeForm.DeleteClick(Event: TJSMouseEvent): Boolean;
begin
  FBookingData.DeleteAll;
  HideDlgDelete;
  RenderBookings;
  ShowWarning('Your local data was removed.');
end;

constructor TTimeForm.Create;
begin
  FBookingData:=TBookingData.Create;

  // Assign Controls
  lblTime:=document.getElementById('lblTime');
  lblActiveBooking:=document.getElementById('lblActiveBooking');
  btnBook:=TJSHTMLElement(document.getElementById('btnBook'));
  btnBook.onclick:=@BookClick;
  btnDelete:=TJSHTMLElement(document.getElementById('btnDelete'));
  btnDelete.onclick:=@DeleteClick;

  // Render Data
  RenderBookings;

  // Set Time timer
  UpdateTime;

  // Show Info Text
  if FBookingData.IsOpened then
    ShowInfo('Press the button to stop your booking')
  else
    ShowInfo('Press the button to start your booking');
end;

procedure TTimeForm.ShowInfo(Text: String; Success: Boolean);
begin
  //Shows a text in the alert box
  if Success then ShowText(Text, 'alert-success')
  else ShowText(Text, 'alert-primary');
end;

procedure TTimeForm.ShowWarning(Text: String);
begin
  ShowText(Text, 'alert-warning');
end;

procedure TTimeForm.ShowError(Text: String);
begin
  ShowText(Text, 'alert-danger');
end;


var TimeForm: TTimeForm;
begin
  TimeForm:=TTimeForm.Create;
end.

