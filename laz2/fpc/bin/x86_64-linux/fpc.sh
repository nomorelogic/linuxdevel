#!/bin/sh
# This script starts the fpc compiler installed by fpcup
# and ignores any system-wide fpc.cfg files
# Note: maintained by fpcup; do not edit directly, your edits will be lost.
/home/lazuser/devel/laz2/fpc/bin/x86_64-linux/fpc -n @/home/lazuser/devel/laz2/fpc/bin/x86_64-linux/fpc.cfg "$@"
